package org.l0nax.simplernavigation

import android.app.Application
import org.l0nax.simplernavigation.database.AppDatabase

class SimplerNavigationApplication: Application() {
    val database: AppDatabase by lazy { AppDatabase.getDatabase(this) }

}