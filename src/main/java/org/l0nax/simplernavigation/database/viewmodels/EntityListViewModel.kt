package org.l0nax.simplernavigation.database.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.flow.Flow
import org.l0nax.simplernavigation.database.entity.Entity
import org.l0nax.simplernavigation.database.entity.EntityDao
import org.l0nax.simplernavigation.database.entity.Module

class EntityListViewModel(private val entityDao: EntityDao) : ViewModel() {

    fun fullSchedule(): List<Entity> = entityDao.getAll()
}

class EntityListViewModelFactory(private val entityDao: EntityDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EntityListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EntityListViewModel(entityDao) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}