package org.l0nax.simplernavigation.database.entity

@Suppress("unused")
enum class Module {
    NONE,
    SIMPLE_CONTACT;
}