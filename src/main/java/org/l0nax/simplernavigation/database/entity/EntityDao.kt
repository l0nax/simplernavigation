package org.l0nax.simplernavigation.database.entity

import androidx.room.Dao
import androidx.room.Query

@Dao
interface EntityDao {
    @Query("SELECT * FROM entity ORDER BY position ASC")
    fun getAll(): List<Entity>
}