package org.l0nax.simplernavigation.database

import android.content.Context
import androidx.room.*
import org.l0nax.simplernavigation.database.entity.Entity
import org.l0nax.simplernavigation.database.entity.EntityDao

@Database(entities = [Entity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun entityDao() : EntityDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(ctx: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    ctx,
                    AppDatabase::class.java,
                    "app_database")
                    .build()
//                    .createFromAsset("database/entity.db")

                INSTANCE = instance

                instance
            }
        }
    }
}